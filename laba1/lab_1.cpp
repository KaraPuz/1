﻿#include <iostream>
#include <string>
#include <stack>


int main()
{
    setlocale(LC_ALL, "Rus");//русификация 

    std::stack <char> subsequence;// стэк для скобок
    std::string line_subque; //строка скобок 
    std::cout << "введите последовательность (subsequence)" << std::endl;
    std::cin >> line_subque;

    for (int i = 0; i < line_subque.length(); i++) //пока проверяемый символ меньше длины строки - проверяем дальше
    {
        char current = line_subque[i]; //пусть текущий элемент будет  iтым в последовательности скобок
        if (current == '(' || current == '[' || current == '{')//проверка на наличие открывающей скобки 
            subsequence.push(current);//в последовательность мы добавляем текущий(current)  элемент, с помощью функции push()
        else if (subsequence.empty()) // чтобы проверить пуст ли стек мы воспользовались функцией empty()
						  //если не будет этой проверки то в случае когда в конце последовательности будет 
                          //одна из закрывающих скобок, программа в попытке получить вершину стека (в которой ничего нет) упадёт
						  
        {
            subsequence.push(current);
            break;
        }
		//проверки на парность скобок
        else if (current == ')') 
        {
            if (subsequence.top() == '(') //если верхний элемент равен такой то скобке 
                subsequence.pop(); //функцию pop() используют для удаления верхнего элемента стека
				                   //(то есть,пара сошлась и мы ее исключаем из последовательности)
            else
                break;
        }
        else if (current == ']')
        {
            if (subsequence.top() == '[')
                subsequence.pop();
            else
                break;
        }
        else if (current == '}')
        {
            if (subsequence.top() == '{')
                subsequence.pop();
            else
                break;
        }
    }

    if (!subsequence.empty())
        std::cout << "скобочная последовательность неправильная" << std::endl;
    else 
        std::cout << "скобочная последовательность правильная" << std::endl;

    return 0;
}
