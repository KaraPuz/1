      //ПОРАЗРЯДНАЯ СОРТИРОВКА
//сложность в любом из случае: O(kn),
// где n - кол-во элементов, каждый из которых состоит из k цифр или символов

#include <iostream>
#include <chrono>


#define N 10
#define MAX_DIGIT 4 //digit цифра, number число


int minusSize(int number, int size)
{
    while (size > 1)
    {
        number /= 10;
        size--;
    }
    return number % 10;
}

//Элементы перебираются по порядку и группируются по самому младшему разряду(или, наоборот,по старшему)
//(сначала все, заканчивающиеся на 0, затем заканчивающиеся на 1, ..., заканчивающиеся на 9). 
//Возникает новая последовательность. Затем группируются по следующему разряду с конца, 
//затем по следующему и т.д. пока не будут перебраны все разряды, от младших к старшим.

void radixSort(int dop_mas[N][N], int mas[N], int size)
{
    int mas_string[N], temp = 0;
    for (int i = 0; i < N; i++) //зануляем все элементы
        mas_string[i] = 0;//
    for (int i = 0; i < N; i++) //идём по элементам массива который сортируем
    {
        int j = minusSize(mas[i], size);// присваиваем j минусРазмер от элемента массива 
        dop_mas[mas_string[j]][j] = mas[i];//какой бы элемент мы не взли из массива mas_string он будет 0 
        mas_string[j]++;//увеличиваем для перехода на следующую строку
    }
    for (int i = 0; i < N; i++)
        for (int k = 0; k < mas_string[i]; k++)
        {
            mas[temp] = dop_mas[k][i];
            temp++;
        }
};


int main()
{

    int mas[N] = { 100, 3, 456, 784, 254, 64, 30, 1238, 6542, 1239 };
    int dop_mas[N][N];
    for (int size = 1; size < MAX_DIGIT + 1; size++)
        radixSort(dop_mas, mas, size);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << " " ;
    return 0;
}

