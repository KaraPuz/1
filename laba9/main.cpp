   //ПИРАМИДАЛЬНАЯ СОРТИРОВКА
//В лучшем случае: O(nlog(n))
//В среднем случае: O(nlog(n))
//В худшем случае: O(nlog(n))

#include <iostream>
#include <chrono>

#define N 15

//корень - "вершина пирамидки" (меняется в процессе сортировки)
void binaryHeap(int arr[], int n, int i)// i- корень и индекс в массиве, н - размер кучи
{
    int largest = i; // Инициализируем наибольший элемент как корень
    int l = 2 * i + 1; // левый 
    int r = 2 * i + 2; // правый 

    if (l < n && arr[l] > arr[largest])// Если левый элемент больше корня
        largest = l;

    if (r < n && arr[r] > arr[largest])// Если правый элемент больше, чем самый большой
        largest = r;

    if (largest != i)// Если самый большой элемент не корень
    {
        std::swap(arr[i], arr[largest]);

        binaryHeap (arr, n, largest);
    }
}

//Алгоритм пирамидальной сортировки в порядке по возрастанию:
//Постройте max-heap из входных данных.
//На данном этапе самый большой элемент хранится в корне кучи. 
//Замените его на последний элемент кучи, а затем уменьшите ее размер на 1. 
//Наконец, преобразуйте полученное дерево в max-heap с новым корнем.
//Повторяйте вышеуказанные шаги, пока размер кучи больше 1

void Heap(int arr[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
        binaryHeap(arr, n, i);// Построение кучи 

    for (int i = n - 1; i >= 0; i--)//извлекаем элемент из кучи (по одному)
    {
        std::swap(arr[0], arr[i]);// Перемещаем текущий корень в конец

        binaryHeap(arr, i, 0); 
    }
}


int main() {
	srand(time(0));
	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = rand() % 100;
	for (int i = 0; i < N; i++)
		std::cout << mas[i] << std::endl;
	std::cout << "=================" << std::endl;
    Heap(mas,N);
    for (int i = 0; i < N; i++)
        std::cout << mas[i] << std::endl;
	return 0;
}

